import strutils, os, osproc, webview  # nimble install webview
                                      # nim c -r machecker_webview.nim
const achetemele = "data:text/html," & """
<center>
  <h1>Machecker WebView</h1> <hr>
  <h3>Carpeta con MP3</h3>
  <input type="url" id="origen"  name="origen" required style="width:99%" value="$1">   <hr>
  <h3>Carpeta de Cuarentena</h3>
  <input type="url" id="destino"  name="destino" required style="width:99%" value="$2"> <hr>
  <button onclick="bidirectional.corre(document.querySelector('#destino').value + ',' + document.querySelector('#origen').value)"> Ejecutar </button>
</center> """.strip.unindent  # HTML/CSS/JS pasado en la URL directamente.

let ventanita = newWebView("Machecker", url = achetemele.format(getCurrentDir(), getTempDir()), resizable = true, debug = not defined(release))
ventanita.bindProcs(scope = "bidirectional"):  # Enganchaditos Bidireccional, Izquierda Web, Derecha Nativo
  proc corre(des_ori: string) = ( proc = quit(execCmdEx("./machecker --carpeta=" & des_ori.split(",")[0].quoteShell & " " & des_ori.split(",")[1].quoteShell).exitCode) )()
ventanita.run()  # ^ Web        ^ Nativo
ventanita.exit()
