import strformat, os, osproc
import ui  # nimble install ui

const
  CARPETA_MOVER_POR_DEFECTO = getTempDir()
  MACHECKER = "./machecker"  # Si esta agregado nimble al PATH deberia encontrarlo, sino copiar el binario en la misma carpeta.


proc corre_wachin(vuleano: bool, carpeta: string, emepetre: string): tuple =
  let opcion = if vuleano: "--offline" else: "--online"
  let comando = fmt"{MACHECKER} {opcion} --carpeta='{carpeta}' '{emepetre}'"
  echo comando
  execCmdEx(comando)


proc main*() =
  let menu = newMenu("Archivo")
  menu.addQuitItem(proc(): bool {.closure.} = return true)

  let ventana_principal = newWindow("Machecker Ventana", 480, 240, true)
  let cajita = newVerticalBox(true)
  ventana_principal.setChild(cajita)

  var carpeta = newEntry(CARPETA_MOVER_POR_DEFECTO)
  var emepe13 = newEntry(getCurrentDir())
  var chekbox = newCheckbox("Funcionar sin Internet (Offline, Local)")
  cajita.add newLabel("Carpeta con MP3")
  cajita.add emepe13
  cajita.add newLabel("Carpeta donde mover los archivos sospechosos")
  cajita.add carpeta
  cajita.add chekbox

  cajita.add newButton("Ejecutar Machecker", proc() = msgBox(
    ventana_principal, "Machecker Ventana - Resultados de Ejecucion",
    corre_wachin(chekbox.checked, carpeta.text, emepe13.text).output))

  show(ventana_principal)
  mainLoop()


init()
main()
