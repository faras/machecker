#!/usr/bin/env nim
#
# nim e build_machecker.nims
import os, strutils

const gccWin32 = "/usr/bin/x86_64-w64-mingw32-gcc"

template upx(binario: string) {.used.} =
  when defined(linux): exec "upx --best --ultra-brute " & binario

template sumaVerificacion(binario: string) =
  echo "Suma de Verificacion\t", binario
  exec "sha512sum --tag " & binario & " > " & binario & ".sha512"

template soloLectura(carpeta: string) =
  for file in walkDir(carpeta):
    if file.kind == pcFile:
      echo "Solo Lectura\t", file
      exec "chmod -w " & file.path

template borraCarpetaDownloads() =
  for file in walkDir("downloads/"):
    if file.kind == pcFile:
      echo "Borrando Archivo\t", file
      rmFile file.path

template desnudar(binario: string) =
  echo "Desnudando Binario\t", binario
  exec "strip --strip-all --remove-section=.comment --remove-section=.note.gnu.gold-version --remove-section=.note --remove-section=.note.gnu.build-id --remove-section=.note.ABI-tag " & binario

template compilaLinux(archivo: string) =
  exec "nim c -d:release -d:danger --threads:on --opt:size --passL:-s --passC:-flto --out:" & archivo.replace("src/", "downloads/").replace(".nim", "") & " " & archivo

template compilaLinux32(archivo: string) =
  exec "nim c -d:release -d:danger --threads:on --opt:size --passL:-s --passC:-flto --cpu:i386 --passC:-m32 --passL:-m32 --out:" & archivo.replace("src/", "downloads/").replace(".nim", "32bit") & " " & archivo

template compilaWindows(archivo: string) =
  exec "nim c -d:release -d:ssl -d:useLibUiDll --opt:size --passL:-static --cpu:amd64 --os:windows --gcc.exe:" & gccWin32 & " --gcc.linkerexe:" & gccWin32 & " --out:" & archivo.replace("src/", "downloads/").replace(".nim", ".exe") & " " & archivo




borraCarpetaDownloads()

echo "Compilando Machecker Linux"
compilaLinux "src/machecker.nim"
desnudar "downloads/machecker"
sumaVerificacion "downloads/machecker"

echo "Compilando Machecker Linux 32Bit"
compilaLinux32 "src/machecker.nim"
desnudar "downloads/machecker32bit"
sumaVerificacion "downloads/machecker32bit"

echo "Compilando Machecker WebView"
compilaLinux "src/machecker_webview.nim"
desnudar "downloads/machecker_webview"
sumaVerificacion "downloads/machecker_webview"

echo "Compilando Machecker Ventana"
compilaLinux "src/machecker_ventana.nim"
desnudar "downloads/machecker_ventana"
sumaVerificacion "downloads/machecker_ventana"

echo "Compilando Machecker Windows"
compilaWindows "src/machecker.nim"
sumaVerificacion "downloads/machecker.exe"

echo "Compilando Machecker Ventana Windows" # Windows EXE. La GUI requiere tener "libui.dll" en la misma carpeta.
compilaWindows "src/machecker_ventana.nim"
sumaVerificacion "downloads/machecker_ventana.exe"

soloLectura "downloads"
