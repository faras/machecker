## **Machecker**
## =============
## (documentación desactualizada, revisa Readme.rst)
## Iniciativa para crear un sistema para verificar si el artista que estas escuchando o a punto de emitir es machirulo.
##
## .. image:: https://source.unsplash.com/FqkBXo2Nkq0/850x420
##
## Producto mínimo viable
## ----------------------
## Cualquier sistema que permita filtrar un listado de artistas por aquellos que tienen denuncias de violencia de genero.
## En principio se trabajara con el listado publicado en tuidoloesunforro.com.ar
## y haciendo un sistema que permita chequear un listado de MP3 de una carpeta contra esa lista.
##
## Requisitos
## ----------
## - FFMpeg https://www.ffmpeg.org `sudo apt install nim ffmpeg`
## - Nim https://nim-lang.org ``>= 0.20.0``
##
## Optimizacion Offline
## --------------------
## Si lo vas a usar siempre offline *(sin HTTPS/SSL)*, podes sacarle el ``-d:ssl`` y ahorra ~25Kb.
##
## Optimizacion Online
## --------------------
## Si lo vas a usar siempre online *(con HTTPS/SSL)*, podes borrar la linea que dice ``localEmbedJson* = staticRead("../forros_filter.json")`` y ahorra el peso del JSON incrustado.
##
## Compilacion desarrollo
## ----------------------
## Hacks, pruebas, testing, desarrollo, etc *(1 Ejecutable ~1Mb)*
## - ``nim c -r -d:ssl machecker.nim "~/ruta/a/carpeta/de/musica/"``
##
## Compilacion Final
## -----------------
## *(1 Ejecutable Standalone ~100Kb)*
## 1. ``nim c -d:ssl -d:release --app:console --opt:size machecker.nim``
## 3. ``./machecker "~/ruta/a/carpeta/de/musica/"``
##
## Como usarlo
## -----------
## - ``./machecker "~/ruta/a/carpeta/de/musica/"``
##
## Instalacion
## -----------
## Como instalarlo en el sistema.
## - ``nimble install https://0xacab.org/faras/machecker.git``
##
## Performance Profiling
## ---------------------
## 1. Descomentar la linea que dice ``import nimprof`` en ``machecker.nim``
## 2. ``nim c --profiler:on --stacktrace:on -d:ssl -d:release --app:console --opt:size machecker.nim``
## 3. ``./machecker "~/ruta/a/carpeta/de/musica/"`` y abrir ``profile_results.txt``
##
## Parametros de Linea de Comando
## ------------------------------
## - ``--licencia`` Ver la Licencia.
## - ``--version`` Ver la Version.
## - ``--offline`` Funcionar Offline con archivo JSON local.
## - ``--online``  Funcionar Online con archivo JSON remoto.
## - ``--offlinemini`` Funcionar Offline con archivo JSON de Desarrollo. (Opcion solo para Desarrollo).
## - ``--remotemini``  Funcionar Online con archivo JSON de Desarrollo. (Opcion solo para Desarrollo).
## - ``--remote`` Funcionar Online con archivo JSON remoto.
## - ``--carpeta=/una/carpeta/local`` Ruta a carpeta local donde mover los archivos con machis.
##
## Generar Documentacion
## ---------------------
## - ``nim doc machecker.nim``
##
## Chequear el Paquete
## -------------------
## Como chequear que el paquete es valido (Linter).
## - ``nimble check``
## - ``nim check src/machecker.nim``
##
## Scrappeando tuidoloesunforro
## ----------------------------
## - Acceder a tuidoloesunforro.com.ar
## - Scrollear hasta abajo varias veces hasta que no cargue más artistas (el último es woody allen o john lennon).
## - Abrir la consola de desarrollador con F12.
## - Pegar el siguiente código:
##
## .. code:: javascript
##    let forros = jQuery("h3 .item-link");
##    var forros_filters = [];
##    for (f in forros) {
##      if (forros[f].innerText) {
##        filter = {artist_name: forros[f].innerText, source: forros[f].href};
##        forros_filters.push(filter)
##      }
##    };
##    let forros_filter_text = JSON.stringify(forros_filters);
##    console.log(forros_filter_text);
##
## - Con botón derecho en la consola elegir "Copiar el objeto" (en Firefox).
## - Crear un archivo nuevo en un editor de texto y pegar el contenido.
## - Grabarlo como ``forros_filter.json``
##
## Colaboración
## ------------
## Para ayudar a avanzar este proyecto se pueden enviar issues en este Gitlab o
## emails a faras@partidopirata.com.ar
##
## Todos los espacios e interacciones de este proyecto aspiran a ser lo más inclusivos posibles y
## se rigen según el `código de conducta`_ (sugerencia de @fauno).
##
## .. _código de conducta: code-of-conduct.md
##
## :Licencia: GPLv3.
## :Repo: https://0xacab.org/faras/machecker
## :COC:  https://0xacab.org/faras/machecker/blob/master/code-of-conduct.md

import json, os, strformat, strutils, httpclient, terminal, times, parseopt, random
when defined(libreria): import ffmpeg else: import osproc

const
  localUrlJson* = "forros_filter.json"                                                                         ## Path local al JSON Final de Produccion ``./forros_filter.json``
  localUrlJsonTesting* = "forros_filter_mini.json"                                                             ## Path local al JSON Temporal de Desarrollo ``./forros_filter_mini.json``
  remoteUrlJson* = "https://0xacab.org/faras/machecker/raw/master/forros_filter.json"                          ## URL al JSON Final de Produccion.
  remoteUrlJsonTesting* = "https://0xacab.org/faras/machecker/raw/master/forros_filter_mini.json"              ## URL al JSON Temporal de Desarrollo.
  ffProbe* = "ffprobe -loglevel error -show_entries format_tags=artist -of default=noprint_wrappers=1:nokey=1" ## Copipasteado desde https://askubuntu.com/a/754922
  localEmbedJson* = staticRead("../forros_filter.json").strip.unindent(9)                                      ## JSON Incrustado en el ejecutable al compilar.
  NimblePkgVersion {.strdefine.} = "0.2.0"                                                                     ## Version (SemVer) lo obtiene automaticamente desde el archivo machecker.nimble

var
  urlJson* = remoteUrlJson ## La URL de JSON por Defecto es la Incrustada.
  movidos* = getTempDir()  ## La Ruta donde mover los archivos con machis.
  cantidad*: int           ## La cantidad total de archivos revisados.


proc main*(carpetas: string, jotason = urlJson, lectorId3tag = ffProbe, carpetaMover = movidos): bool =
  ## **Machecher** toma 1 ruta completa a una carpeta y una URL a 1 JSON,
  ## chequea recursivamente los ID3 Tag de todos los archivos ``*.mp3`` contra un JSON.
  ##
  ## Imprime el resultado en la terminal, finaliza bien si esta todo bien.
  assert existsDir(carpetas), "La carpeta de origen no existe." # assert son removidos cuando se compila para Release
  assert existsDir(carpetaMover), "La carpeta de destino no existe."
  assert jotason.len > 1, "jotason no debe ser un string vacio."
  assert lectorId3tag.len > 1, "lectorId3tag no debe ser un string vacio."

  var                         ## Aca van las variables que usa el for loop
    outputStr, output: string # Inicializado a ""
    archivoConMachi: bool     # Inicializado a false
    exitCodeInt: int          # Inicializado a 0
    machis: JsonNode

  let clientito = newHttpClient()

  try: ## JSON puede ser 1 ruta completa al archivo local o 1 link HTTP / HTTPS de Internet.
    machis = parseJson(if jotason.startswith("http"): clientito.getContent(jotason) else: readFile(jotason))
  except:
    echo "No se puede obtener JSON Remoto. No se puede obtener JSON Local. Usando el JSON Incrustado."
    machis = parseJson(localEmbedJson) # Usa JSON Incrustado.

  ## Colores de la Terminal.
  randomize()
  setBackgroundColor(bgBlack)
  setForegroundColor([fgRed, fgGreen, fgYellow, fgBlue, fgMagenta, fgCyan].sample)
  defer: resetAttributes() # Reset colores de la Terminal.
  echo "Utilizando filtros de archivo:\t", jotason
  echo "Machequeando la carpeta:\t", carpetas

  ## Loop recursivo a todos los archivos, obtiene ID3Tags, compara versus todos los machis.
  for emepetrece in walkDirRec(carpetas):
    var emepetre = expandFilename(emepetrece) # De path relativo a full path.
    var testId3 = false
    archivoConMachi = false
    inc cantidad

    ## Si es MP3 obtener metadatos en id3 tags
    if emepetre.toLowerAscii.endswith(".mp3"):
      echo "Machequeando archivo:\t", emepetre

      when defined(libreria):
        ## https://github.com/mashingan/nimffmpeg/blob/master/examples/metadata.nim
        exitCodeInt = 1
        var
          tag: ptr AVDictionaryEntry
          formato: ptr AVFormatContext
          retval = avformat_open_input(addr formato, emepetre.cstring, nil, nil)
        if retval != 0: exitCodeInt = 666 # No se puede leer el MP3 ?.
        retval = avformat_find_stream_info(formato, nil)
        if retval < 0: exitCodeInt = 666  # MP3 sin Metadata ?.
        while true:
          tag = av_dict_get(formato[].metadata, "", tag, AV_DICT_IGNORE_SUFFIX)
          if tag == nil: break
          if $tag[].key == "artist": outputStr = $tag[].value
        avformat_close_input(addr formato)
      else:
        ## llama el comando FFProbe, obtiene artista, limpia el resultado.
        (outputStr, exitCodeInt) = execCmdEx(fmt"{lectorId3tag} '{emepetre}'")

      output = $outputStr.strip.toLowerAscii
      ## Segun el resultado, muestra un mensaje.
      if not exitCodeInt == 1:
        echo "FFProbe fallo en el archivo:\t", emepetre, exitCodeInt, output
      elif output == "":
        echo "Archivo sin ID3 Tags:\t", emepetre
      else:
        testId3 = true

    for machi in machis:
      var artista = $machi["artist_name"].str.strip.toLowerAscii

      ## Testear metadatos id3 si hay.
      if testId3 and artista in output:
        echo fmt"Archivo con machis en ID3 Tag!: {machi} --> {emepetre}"
        result = true # retorna true si hay machis en id3, false si no se encontro ninguno.
        archivoConMachi = true # result es total, archivoConMachi es por archivo.

      ## Testear nombre de archivo.
      if emepetre.toLowerAscii.find(artista) > -1:
        echo fmt"Archivo con machis en nombre de archivo!: {machi} --> {emepetre}"
        result = true # retorna true si hay machis en nombre de archivo, false si no se encontro ninguno.
        archivoConMachi = true

    ## Mover archivo.
    if archivoConMachi:
      var emepetreMovido = carpetaMover / splitPath(emepetre).tail
      echo fmt"Archivo movido: {emepetre} --> {emepetreMovido}"
      moveFile(emepetre, emepetreMovido)


when isMainModule:
  # Sin ningun parametro no funciona, no sabe que hacer, imprime mensaje y sale.
  const ayudin = """Por favor invocar:  ./machecker [ruta_completa_a_carpeta_con_mp3]

  --licencia                 Ver la Licencia.
  --version                  Ver la Version.
  --offline                  Funcionar Offline con archivo JSON local.
  --online                   Funcionar Online con archivo JSON remoto.
  --remote                   Funcionar Online con archivo JSON remoto.
  --carpeta='/carpeta/local' Ruta a carpeta local donde mover los archivos con machis.
  --offlinemini              Funcionar Offline con archivo JSON de Desarrollo. (Opcion solo para Desarrollo).
  --remotemini               Funcionar Online con archivo JSON de Desarrollo. (Opcion solo para Desarrollo).
  --desinstalar              Desinstalar, se borra a si mismo del disco (No se puede deshacer).

  """

  if paramCount() == 0: quit(ayudin)

  # Parsea los parametros de la linea de comandos.
  for tipoDeClave, clave, valor in getopt():
    case tipoDeClave
    of cmdShortOption, cmdLongOption:
      case clave
      of "version": quit(NimblePkgVersion, 0)
      of "license", "licencia": quit("GPLv3", 0)
      of "help", "ayuda": quit(ayudin, 0)
      of "local", "offline": urlJson = localUrlJson
      of "localmini", "offlinemini": urlJson = localUrlJsonTesting
      of "remotemini", "onlinemini": urlJson = remoteUrlJsonTesting
      of "remote", "online": urlJson = remoteUrlJson
      of "carpeta": movidos = valor
      of "desinstalar": discard tryRemoveFile(currentSourcePath()[0..^5])
    of cmdArgument:
      echo "Machequeo iniciando:\t", $now()
      echo main(clave) # clave es la carpeta/url.
      echo "Machequeo terminado:\t", $now(), ", archivos revisados ", cantidad
    of cmdEnd: quit("Malos argumentos, por favor ver la ayuda con: --ayuda", 1)


when defined(windows): {.hint: "Se recomienda migrar a GNU/Linux".}
when not defined(ssl): {.warning: "Sin SSL no funciona el HTTPS!".}
when not defined(release): {.hint: "Compilar con -d:release para Produccion".}
