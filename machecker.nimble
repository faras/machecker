# Package
version       = "0.2.0"
author        = "Faras"
description   = "Iniciativa para crear un sistema para verificar si el artista que estas escuchando o a punto de emitir es machirulo."
license       = "GPLv3"
srcDir        = "src"
bin           = @["machecker"]


# Dependencies
requires "nim >= 1.0.6"
requires "ui"       # GUI Nativa
requires "webview"  # GUI WebView
requires "https://github.com/mashingan/nimffmpeg.git"  # Libreria FFMPEG Nativa


# Pre-Pre-Install
import distros

task setup, "Setup mas kawai":
  if not defined(ssl):
    echo "SSL no definido: SSL deshabilitado, HTTPS deshabilitado."

  if detectOs(Ubuntu):
    echo "Ubuntu detectado"
    foreignDep "gcc-multilib"
    foreignDep "build-essential"
    foreignDep "upx-ucl"
    foreignDep "git"
    foreignDep "curl"
    foreignDep "gcc-mingw-w64"
    foreignDep "ffmpeg"
    # foreignDep "wine"


before install:
  setupTask()
