**Machecker**
=============

* **Verifica si el artista que estas escuchando o emitiendo tiene cargos por Violacion o Abuso Sexual.**

.. image:: https://source.unsplash.com/FqkBXo2Nkq0/850x420

`Boicot a los músicos violadores <https://martinszy.github.io/automatizar-el-boicot/#/9>`_

.. image:: machecker_webview.png


Estado del proyecto
----------------------

* Machecker es actualmente un Producto mínimo viable, se requiere apoyo para agregar funcionalidades y difundirlo.
* Permite filtrar una colección de archivos, por aquellos que tienen en su nombre o en su TAG ID3 (como los archivos .mp3) como creadores a personas que tienen denuncias de violencia de genero.
* Trabaja con el listado publicado en http://tuidoloesunforro.com.ar
* Mueve los archivos a otra carpeta, para quitarlos de las listas de reproducción y facilitar su eventual eliminación.
* No elimina los archivos.
* Queremos agregar muchas más funcionalidades revisar en los `issues <https://0xacab.org/faras/machecker/issues>`_
* Tiene una interface grafica WebView HTML5. Tiene una interface grafica Nativa.
* Fue probado en radios comunitarias con éxito.

Instalación
-----------
* Descargar la versión para windows o para linux desde aquí: https://0xacab.org/faras/machecker/tree/master/downloads
* Comprobar Suma de Verificacion de archivos descargados para limitar el riesgo de haber recibido un archivo modificado (Opcional).
* En linux, dar permisos de ejecución al archivo ``chmod +x machecker`` o ``chmod +x machecker_webview``

Como Verificar SHA512
---------------------

``sha512sum --check machecker.sha512``

* Dice OK si esta bien.
* Se asume ``machecker.sha512`` como nombre de archivo en el ejemplo, cambiar segun el caso de uso.

Como usarlo
-----------

Linux:
* Versión sin ventanas: ``./machecker "~/ruta/a/carpeta/de/musica/"``

Windows:
* Versión sin ventanas:
** ejecutar cmd.exe o powershell
** ejecutar `machecker.exe`


Parámetros de linea de comando
------------------------------
Estos parámetros funcionan de igual forma en todas las versiones.

- ``--licencia`` Ver la Licencia.
- ``--version`` Ver la Version.
- ``--offline`` Funcionar Offline con archivo JSON local.
- ``--online``  Funcionar Online con archivo JSON remoto.
- ``--remote`` Funcionar Online con archivo JSON remoto.
- ``--carpeta=/una/carpeta/local`` Ruta a carpeta local donde mover los archivos con machis.
- ``--offlinemini`` Funcionar Offline con archivo JSON de Desarrollo. (Opcion solo para Desarrollo).
- ``--remotemini``  Funcionar Online con archivo JSON de Desarrollo. (Opcion solo para Desarrollo).
- ``--desinstalar`` Desinstalar, se borra a si mismo del disco. (Opcion avanzada).

Optimizacion Offline: Si lo vas a usar siempre offline *(sin HTTPS/SSL)*, podes sacarle el ``-d:ssl`` y ahorra ~25Kb.

Optimizacion Online:Si lo vas a usar siempre online *(con HTTPS/SSL)*, podes comentar la linea que dice

``LOCAL_EMBED_JSON* = staticRead("../forros_filter.json")`` y ahorra el peso del JSON incrustado.


Instalacion para desarrollo
---------------------------

Requisitos:

- FFMpeg https://www.ffmpeg.org
- Nim https://nim-lang.org ``>= 1.0.6``
- UPX para optimización de binarios (Opcional).

Instalación de requisitos con apt:

``sudo apt install nim ffmpeg upx-ucl``

Para actualizar nim:

`choosenim update stable`

Descargar código:

- ``nimble install https://0xacab.org/faras/machecker.git``


Compilación
-----------------

La compilación genera un ejecutable sin dependencias (Standalone) de alrededor de ~100Kb.

1. Compilación: ``nim c -o:downloads/machecker -d:ssl -d:release -d:danger --app:console --opt:size -r src/machecker.nim``
2. Optimización del binario: ``upx --best --ultra-brute ./downloads/machecker``


Usar FFMPEG Libreria Nativa 
---------------------------

Hay 2 formas de compilar Machecker:

- Usa el programa de ``ffmpeg`` por linea de comando como otro subproceso.
- Usa ``ffmpeg`` como Libreria Nativa por la API en C directamente.

Para compilar con FFMPEG con Libreria Nativa se debe agregar al comando de compilacion ``-d:libreria``.

Ejemplo:

- ``nim c -d:ssl -d:release -d:danger -d:libreria src/machecker.nim``

Cuando ``libreria`` esta definido compilara con la libreria FFMPEG Nativa.


Compilacion GUI Experimental
----------------------------

*(~150Kb sin dependencias, crossplatform)*

- ``sudo apt install gtk+-3.0``
- ``nimble install ui``
- ``cd downloads``
- ``nim c -o:./machecker_ventana --threads:on -d:release --opt:size -r ../src/machecker_ventana.nim``
- ``upx --best --ultra-brute ./machecker_ventana``

Como Generar SHA512
-------------------

Ejecutar ``sha512sum --tag machecker > machecker.sha512``

* Se asume ``machecker`` como nombre de archivo en el ejemplo, cambiar segun el caso de uso.


CrossCompile
------------

(documentación incompleta)

**Linux -> Windows** instalar en Linux los siguientes paquetes con todas sus dependencias:

Instala Libs para generar ``*.exe`` en Linux.

``sudo apt install build-essential binutils-mingw-w64 gcc-mingw-w64 mingw-w64-*``


Actualizar base de datos
----------------------------

Los datos con los que trabaja esta aplicación se consiguen scrappeando un sitio llamado tuidoloesunforro, ellas dicen que:
! Cuando no responsabilizamos a nuestros ídolos, cuando los subimos a pedestales, les entregamos premios y reconocimientos y mucha pero mucha plata, estamos diciendo que la violencia hacia la mujer está bien. De aquí parte “Tu ídolo es un forro” en un intento de visibilizar a estos “exitosos abusadores” tanto nacionales como internacionales y de conciliar las acciones violentas de estos hombres con el castigo cultural que merecen recibir.

* Ejecutar `nim -c -r src/machecker_refrescador.nim`
* Genera una nueva versión de forros_filter.json
* Usar la nueva lista localmente, revisar opciones de línea de comando.
* Para distribuir la lista actualizada, recompilar los binarios y enviar un merge request.


Pruebas
----------------------

Usar este método para verificar el funcionamiento de machecker antes de realizar un merge request, un nuevo release o para probar hacks. Genera un ejecutable ~1Mb, no es para redistribución.

- Ejecutar sobre archivos de prueba: ``nim c -r -d:ssl machecker.nim "./test_files``
- Recuperar archivos de prueba movidos: ``git checkout test_files``

Usar este método para verificar con un listado local, por ejemplo antes de enviar correcciones al listado de forros.

-  nim c -r -d:ssl machecker.nim --offline "./test_files`


Performance Profiling
---------------------

1. Descomentar la linea que dice ``import nimprof`` en ``machecker.nim``
2. ``nim c --profiler:on --stacktrace:on -d:ssl -d:release --app:console --opt:size machecker.nim``
3. ``./machecker "~/ruta/a/carpeta/de/musica/"`` y abrir ``profile_results.txt``


Generar Documentacion
---------------------

- ``nim doc machecker.nim``


Chequear el Paquete
-------------------

Como chequear que el paquete es valido (Linter).

- ``nimble check``
- ``nim check machecker.nim``


Embellecer el Paquete
---------------------

Como auto-formato o code beautifier (Prettify).

- ``nimpretty machecker.nim``


Colaboración
------------

Para ayudar a avanzar este proyecto se pueden enviar issues en este Gitlab.

Todos los espacios e interacciones de este proyecto aspiran a ser lo más inclusivos posibles y
se rigen según el `código de conducta`_ (gracias @fauno por la sugerencia).

.. _código de conducta: code-of-conduct.md


:Licencia del código: GPLv3.
:Licencia de la documentación: PPL.
:Repositorio: https://0xacab.org/faras/machecker
:Código de conducta:  https://0xacab.org/faras/machecker/blob/master/code-of-conduct.md
